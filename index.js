const express = require ('express')
const app = express()
var port = process.env.PORT || 8080
app.use (express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
app.listen(port, () =>
console.log('HTTP Server with Express.js is listening on port: '+ port));
app.get('/', (req,res) => {
    res.send('Microservice 2 Gateway by Venkata Krishna Prasanth Budigi and Dineshchoudhari Yarlagadda.');
})
app.get('/days_weeks', function (req,res){
    var start = req.query.start;
    var end =req.query.end;
    var days=end-start;
    var weeks=days/7;
    console.log(weeks)
    console.log(days)
    res.send("The days and weeks between the Start_date:"+start+"and the End_date:"+end+"are:"+days+" and "+weeks+"consequtively")
});
